<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class bookissuemodel extends CI_Model {
    
     public function readdata() {// for reading data from issue_history table
        $sql="select * from issue_history where status = 'pending'";
        $query= $this->db->query($sql);
        return $query->result();         
     }
     
    public function bookapprove($bookid,$userid) {
        //$sql_insert="INSERT INTO `issue_history`(`userid`, `book_id`, `issue_date`, `status`) VALUES ($userid,$bookid,CURRENT_DATE,'pending')";       
        $sql_update_books="UPDATE `books` SET `issued_status`='issued' WHERE `book_id`=$bookid";
        $sql_update_issuehistory="UPDATE `issue_history` SET `status`='approved' WHERE `book_id`=$bookid and `userid`=$userid";
        $books_query= $this->db->query($sql_update_books);
        $issue_query= $this->db->query($sql_update_issuehistory);
        return $issue_query;
    }
    
    public function approval_result() {
        $user= $_SESSION['username'];
        $useridquery = "select userid from users where username = '$user'";
        $userid_query_result = $this->db->query($useridquery);
        $userid= $userid_query_result->row()->userid;
        
        //$sql_approval_result="SELECT books.book_id, book_title FROM `issue_history` INNER JOIN books ON issue_history.book_id = books.book_id where issue_history.userid=$userid" ;
        $sql_approval_result="SELECT books.book_id, book_title FROM `issue_history` INNER JOIN books ON issue_history.book_id = books.book_id WHERE issue_history.userid=$userid AND issue_history.status='approved'";
        $approval_query= $this->db->query($sql_approval_result);
        return $approval_query->result();        
    }
}