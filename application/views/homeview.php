<!--
/* this view file corresponds to the homecontroller - view for post login home screen */
-->
<html>
    <head>
        <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!--script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script-->
        <style>
            .wrapper{
                margin: 10px;
                padding: 20px;
            }
            .wrapper *{
                padding: 10px;
            }
            table, th, td {
                border: 1px solid black;
            }
        </style>
    </head>
    <body onload="hidefunction();">
        Welcome user
        <?php echo $user;?><br>
        <button style="margin-left:auto; float:right;"><a  href="<?php echo base_url()."index.php/logincontroller/logout" ?>">Logout</a></button><br>
        
        <?php
        $path = base_url()."index.php/bookissuecontroller/readdata";
        
        if ($_SESSION['role'] == 'admin') {
            echo 'admin/librarian logged in...'.'<br><script>$(document).ready(function(){$(".request").hide();$(".approvedlist").hide();});</script>';
            echo("<button type= 'submit' style='margin-left:400px;'><a href='$path'>Pending Book Requests</a></button>");
        } else {
            echo 'employee/member logged in'.'<br><script>function hidefunction(){document.getElementById("bookinput").style.visibility = "collapse";document.getElementById("userlist").style.visibility = "collapse";document.getElementById("userinput").style.visibility = "collapse";}</script>';
        }
        ?>
        <div name ="overall" class="wrapper" id="mywrapper">
            <div name="approvedlist" class="approvedlist">
                <br><h3>The following books have been approved</h3><br>
                <?php
                    echo '<br><table><th>bookid</th><th>book title</th>';
                    foreach ($approval_result as $key => $value) {
                        echo '<tr>';
                        foreach($value as $v){
                            echo ' <td>'.$v.' </td>';
                        }
                        echo '</tr>'."";
                    }
                    echo '</table>';
                ?>
            </div>
            <div name="booklist">
                <h1 style="margin-left:100px;">Books</h1>
                <?php
                if(isset($_POST['request'])){
                    if($bookissue){
                        echo "<br> The request for book with id $booknumber has been sent to the librarian for approval <br>";
                        echo "<script></script>";
                    }
                }       
                if(TRUE){
                    $total= count($result);
                    $num_rows=count($result[0]);
                    echo '<br><table><th>id</th><th>book_title</th><th>author</th><th>year</th><th>issued</th>';
                    foreach ($result as $key => $value) {
                        echo form_open("homecontroller/bookrequest").'<tr>';                       
                            echo "<td>$value->book_id</td>";
                            echo "<td>$value->book_title</td>";
                            echo "<td>$value->author_name</td>";
                            echo "<td>$value->year</td>";
                            echo "<td>$value->issued_status</td>";
                            echo "<input type='hidden' value='$value->book_id' name='bookid'>";
                            echo "<input type='hidden' value='$value->book_id' name='$value->book_id'>";
                            if($value->issued_status=='available'){
                        echo "<td class='request' id='$value->book_id'><input type='submit' name='request' value='Request'></input></td>";
                            }
                        echo '</tr></form>';
                    }
                    echo '</table>';
                } ?>
            </div>  
            
            <div name="bookinput" id="bookinput">
            <?php echo form_open("homecontroller/bookinput"); ?>
                <table><tr>
                    <td><input type="text" name="bookid" id="bookid" placeholder="enter book_id"></td>
                    <td><input type="text" name="title" id="title" placeholder="enter book title"></td>
                    <td><input type="text" name="author" id="author" placeholder="enter book author"></td>>
                    <td><input type="text" name="year" id="year" placeholder="enter book year"></td>
                    <td><label for="status">book issue status?</label>
                <select name="status" id="status"><option value="pending">pending</option><option value="available">available</option><option value="issued">issued</option></select></td>
                </tr></table>
                <input type="submit" name="submit" value="save">
            </form>
            </div>
               

            <div name="userlist" id="userlist">
                <h1 style="margin-left:100px;">Users</h1>
                <?php
                if(TRUE){
                    $total2= count($usersresult);
                    $num_columns2=count($usersresult[0]);
                    echo '<br>';
                    echo '<br><table><th>userid</th><th>username</th><th>password</th><th>usertype</th><th>name</th><th>email</th><th>creation_date</th>';
                    foreach ($usersresult as $key => $value) {
                        echo '<tr>';
                        foreach($value as $v){
                            echo ' <td>'.$v.' </td>';
                        }
                        echo '</tr>'."";
                    }
                    echo '</table>';
                }?>
            </div>
            
            <div name="userinput" id="userinput">
                <?php echo form_open("homecontroller/userinput"); ?>
                    <table><tr>
                        <td><input type="text" name="userid" id="bookid" placeholder="enter userid"></td>
                        <td><input type="text" name="username" id="title" placeholder="enter username"></td>
                        <td><input type="text" name="password" id="author" placeholder="enter password"></td>>
                        <td><input type="text" name="usertype" id="usertype" placeholder="enter usertype"></td>
                        <td><input type="text" name="name" id="name" placeholder="enter name"></td>
                        <td><input type="text" name="email" id="email" placeholder="enter email"></td>
                    </tr></table>
                    <input type="submit" name="submit" value="save">
                </form>
            </div>
        </div>       
    </body>
</html>

