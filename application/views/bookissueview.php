<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
            table, th, td {
                border: 1px solid black;
                                margin: 10px;
                padding: 20px;
            }
            .wrapper{
                margin: 10px;
                padding: 20px;
            }
            .wrapper *{
                padding: 10px;
            }
        </style>
    </head>
    <body>
        
        <button class="btn btn-lg " style="margin-right:auto; margin-left:50px; float:left;"><a  href="<?php echo base_url()."index.php/homecontroller/" ?>">Home</a></button>
        <button class="btn btn-lg " style="margin-left:auto; margin-right:50px; float:right;"><a  href="<?php echo base_url()."index.php/logincontroller/logout" ?>">Logout</a></button><br>
        <br><br>Welcome user
        <?php echo $user;?><br>
        <?php if($_SESSION['role'] == 'admin'){
                echo 'admin/librarian logged in...';
               } else {
                echo 'employee/member logged in..';
               } 
               if(isset($_POST['approve'])){
                    if($bookapprove){
                        echo "<br> the request has been approved and the book is issued<br>";
                    }
               }
        ?>    
        
            <div class="wrapper" name="requestlist" id="requestlist">
                <h1 style="margin-left:100px;">Issue history</h1>
                <?php
                if(TRUE){
                    $total= count($result);
                    $num_columns=count($result[0]);
                    echo '<br>';
                    echo '<br><table><th>userid</th><th>bookid</th><th>issuedate</th><th>status</th>';
                    foreach ($result as $key => $value) {
                        echo form_open("bookissuecontroller/bookapprove").'<tr>';    
                        echo "<td>$value->userid</td>";
                        echo "<td>$value->book_id</td>";
                        echo "<td>$value->issue_date</td>";
                        echo "<td>$value->status</td>";
                        echo "<input type='hidden' value='$value->book_id' name='bookid'>";
                        echo "<input type='hidden' value='$value->userid' name='userid'>";
                        echo "<td class='approve' id='$value->book_id'><input type='submit' name='approve' value='Approve book issue'></input></td>";
                        echo '</tr>'."";
                        echo "</form>";
                    }
                    echo '</table>';
                }?>
            </div>
    </body>
</html>    