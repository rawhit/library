-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2018 at 04:45 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(10) NOT NULL,
  `book_title` varchar(200) DEFAULT NULL,
  `author_name` varchar(100) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `is_issued` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `book_title`, `author_name`, `year`, `is_issued`) VALUES
(1, 'godfather', 'mario puzo', 4, 'pending'),
(2, 'othello', 'shakespeare', 3, 'pending'),
(3, 'harry potter', 'j k rowling', 32, 'pending'),
(4, 'hamlet', 'shakespear', 12, 'no'),
(5, 'hamlet', 'shakespear', 12, 'no'),
(6, 'hamlet', 'shakespear jackson', 22, 'yes'),
(7, 'hamlet', 'shakespear jackson', 22, 'yes'),
(8, 'sherlock holmse', 'conan doyle', 200, 'no');

-- --------------------------------------------------------

--
-- Table structure for table `issue_history`
--

CREATE TABLE `issue_history` (
  `userid` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `issue_date` date NOT NULL,
  `status` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issue_history`
--

INSERT INTO `issue_history` (`userid`, `book_id`, `issue_date`, `status`) VALUES
(3, 3, '2018-11-08', ''),
(1, 1, '2018-11-08', 'pending'),
(1, 4, '2018-11-08', 'pending'),
(1, 2, '2018-11-08', 'pending'),
(1, 2, '2018-11-08', 'pending'),
(1, 2, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 3, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 2, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 5, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 3, '2018-11-08', 'pending'),
(1, 6, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 3, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 1, '2018-11-08', 'pending'),
(1, 2, '2018-11-08', 'pending'),
(1, 3, '2018-11-08', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(100) NOT NULL,
  `username` varchar(121) NOT NULL,
  `password` varchar(111) NOT NULL,
  `usertype` enum('admin','employee','librarian') DEFAULT 'employee',
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `creation_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `username`, `password`, `usertype`, `name`, `email`, `creation_date`) VALUES
(1, 'rohithr', 'rohithr', 'employee', NULL, NULL, '2018-11-07 11:51:16'),
(2, 'rahulr', 'rahulr', 'admin', NULL, NULL, '2018-11-07 11:51:16'),
(3, 'vineethv', 'vineethv', 'employee', NULL, NULL, '2018-11-07 11:51:16'),
(4, 'arjuns', 'arjuns', 'librarian', NULL, NULL, '2018-11-07 11:51:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `issue_history`
--
ALTER TABLE `issue_history`
  ADD KEY `userid` (`userid`),
  ADD KEY `book_id` (`book_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `issue_history`
--
ALTER TABLE `issue_history`
  ADD CONSTRAINT `issue_history_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`),
  ADD CONSTRAINT `issue_history_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
