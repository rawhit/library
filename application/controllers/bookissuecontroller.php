<?php
class bookissuecontroller extends CI_controller{

    public function index(){
        $data['user']= $_SESSION['username'];
        $this->load->model('bookissuemodel');
        $data['result']=$this->bookissuemodel->readdata();
        $this->load->view('bookissueview',$data);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function readdata() { // for reading books table
        $data['user']= $_SESSION['username'];
        $this->load->model('bookissuemodel');
        $data['result']=$this->bookissuemodel->readdata();
        $this->load->view('bookissueview',$data); 
    }
    
    public function bookapprove() {
      $data['user']= $_SESSION['username'];
      $bookid= $this->input->post('bookid');
      $userid= $this->input->post('userid');
      $this->load->model('bookissuemodel');  
      
      $data['bookapprove']= $this->bookissuemodel->bookapprove($bookid,$userid);
      $data['result']=$this->bookissuemodel->readdata();
      $this->load->view('bookissueview',$data);
    }
    
    public function approval_result() {
         $data['user']= $_SESSION['username'];
         $this->load->model('homemodel');
         $data['result']=$this->homemodel->readdata();
         $this->load->model('bookissuemodel');
         $data['approval_result']=$this->bookissuemodel->approval_result();
         $this->load->view('homeview',$data);
    }
}    