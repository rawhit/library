<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class homecontroller extends CI_Controller{
    
    var $data;
    
    public function index() {
        $data['user']= $_SESSION['username'];
        $this->load->model('homemodel');
        $data['result']=$this->homemodel->readdata();
        $data['usersresult']=$this->homemodel->readusers();
        
        // this is to display the result of approved book requests to the member. here model used is bookissuemodel
         $this->load->model('bookissuemodel');
         $data['approval_result']=$this->bookissuemodel->approval_result();
         $this->load->view('homeview',$data);
    }
    
    public function __construct() {
        parent::__construct();
    }
     
    public function bookinput() {
        $data['user']= $_SESSION['username'];
        $this->load->model('homemodel');
        $data['inputresult']=$this->homemodel->bookinput();
        $data['result']=$this->homemodel->readdata();
        $data['usersresult']=$this->homemodel->readusers();
        $this->load->view('homeview', $data);
    }   
    
    public function readdata() { // for reading books table
        $data['user']= $_SESSION['username'];
        $this->load->model('homemodel');
        $data['result']=$this->homemodel->readdata();
        $this->load->view('homeview',$data); 
    }   
        
        public function readusers() {
        $data['user']= $_SESSION['username'];
        $this->load->model('homemodel');
        $data['usersresult']=$this->homemodel->readusers();
        $this->load->view('homeview',$data); 
    }  
    
    public function bookrequest() {
        //if(isset($_POST[$value->book_id])){$this->session->set_userdata('request_clicked',$_POST[$value->book_id]);}
        $bookid= $this->input->post('bookid');
        $data['booknumber']=$bookid;
        $data['user']= $_SESSION['username'];
        $this->load->model('homemodel');
        $data['bookissue']=$this->homemodel->bookrequest($bookid);
        $data['result']=$this->homemodel->readdata();
        
        // this is to display the result of approved book requests to the member. here model used is bookissuemodel
         $this->load->model('bookissuemodel');
         $data['approval_result']=$this->bookissuemodel->approval_result();
        
        $this->load->view('homeview',$data);
    }
    

    
    public function userinput() {
        $this->load->model('homemodel');
        //$this->homemodel->userinput();
    }
    
}
