<?php
class logincontroller extends CI_controller{

    public function index(){
        $this->load->view('loginview');
    }

    public function __construct() {
        parent::__construct();
    }

    public function logout() {
    $this->session->sess_destroy();
    $this->load->view('loginview');
    }

    public function loginverify(){
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $this->session->set_userdata('username',$user);

        $this->load->model('loginmodel');
        $this->session->set_userdata('role',$this->loginmodel->get_user_role($_SESSION['username']));

        if($this->loginmodel->checklogin($user,$pass)){
            //$this->load->view('homeview',$data);
            redirect("homecontroller");   
        }else{
            echo 'incorrect login. please check details and try again';
            $this->load->view('loginview');
        }
    }
    
    public function signup() {
        $this->load->view('signupview');
    }
    
    public function create_member(){  //create user account in db
        $this->load->library('form_validation');
                //validation rules
        $this->form_validation->set_rules('firstname', 'Firstname', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('lastname', 'Lastname', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'trim|required|matches[password]');
        //$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[msm_users.username]');
      
        if ($this->form_validation->run() == FALSE) //didnot validate
        {
            $this->load->view('signup');
        }
        else
        {       
            $this->load->model('LoginModel');
            $query= $this->LoginModel->create_account();

            if($query)
            {
                $data['account_created']= 'Your account has been created. <br><br> You may now login';
                $data['is_logged_in']=true;
                $this->load->view('Login',$data);
            }
            else 
            {   
                $this->load->view('Signup'); 
            }
        }
        
    }

}

?>